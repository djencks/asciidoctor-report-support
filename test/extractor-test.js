/* eslint-env mocha */
'use strict'

//Opal is apt to be needed for Antora tests, perhaps not otherwise
// IMPORTANT eagerly load Opal since we'll always be in this context; change String encoding from UTF-16LE to UTF-8
const { Opal } = require('asciidoctor-opal-runtime')
if ('encoding' in String.prototype && String(String.prototype.encoding) !== 'UTF-8') {
  String.prototype.encoding = Opal.const_get_local(Opal.const_get_qualified('::', 'Encoding'), 'UTF_8') // eslint-disable-line
}
// eslint-disable-next-line no-unused-vars
const asciidoctor = require('@asciidoctor/core')()

const { expect } = require('chai')
const { extractor, scSubs } = require('./../lib/reports')._private

describe('extractor tests', () => {
  it('basic', () => {
    const extract = [{ path: 'src.relative', match: 'json/camel-(?<basename>*)-kafka-(?<type>source|sink)-connector.json' }]
    const x = extractor(extract)
    const result = x({ src: { relative: 'json/camel-activemq-kafka-sink-connector.json' } })
    expect(result.basename).to.equal('activemq')
    expect(result.type).to.equal('sink')
  })
})

describe('substitution test', () => {
  it('substitution test', () => {
    expect(scSubs('this is a <value> & <another-value>')).to.equal('this is a &lt;value&gt; &amp; &lt;another-value&gt;')
  })
})
