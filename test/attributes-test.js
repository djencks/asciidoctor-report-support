/* eslint-env mocha */
'use strict'

// IMPORTANT eagerly load Opal since we'll always be in this context; change String encoding from UTF-16LE to UTF-8
const { Opal } = require('asciidoctor-opal-runtime')
if ('encoding' in String.prototype && String(String.prototype.encoding) !== 'UTF-8') {
  String.prototype.encoding = Opal.const_get_local(Opal.const_get_qualified('::', 'Encoding'), 'UTF_8') // eslint-disable-line
}
// eslint-disable-next-line no-unused-vars
const asciidoctor = require('@asciidoctor/core')()
const { expect } = require('chai')

const { requiredAttributes, checkAttributes } = require('./../lib/reports')._private

describe('requiredAttributes tests', () => {
  it('requiredAttributes all true', () => {
    const req = requiredAttributes({ foo: true, bar: true })
    expect(req).to.deep.equal(['foo', 'bar'])
  })
  it('requiredAttributes with literal false', () => {
    const req = requiredAttributes({ foo: true, bar: false })
    expect(req).to.deep.equal(['foo'])
  })
  it('requiredAttributes with parameter false', () => {
    const barish = false
    const req = requiredAttributes({ foo: true, bar: barish })
    expect(req).to.deep.equal(['foo'])
  })
  it('requiredAttributes with parameter undefined', () => {
    const barish = undefined
    const req = requiredAttributes({ foo: true, bar: barish })
    expect(req).to.deep.equal(['foo'])
  })
  it('requiredAttributes with parameter defined', () => {
    const barish = 'false'
    const req = requiredAttributes({ foo: true, bar: barish })
    expect(req).to.deep.equal(['foo', 'bar'])
  })
  it('requiredAttributes with negated parameter false', () => {
    const barish = true
    const req = requiredAttributes({ foo: true, bar: !barish })
    expect(req).to.deep.equal(['foo'])
  })
  it('requiredAttributes with negated parameter undefined', () => {
    const barish = undefined
    const req = requiredAttributes({ foo: true, bar: !barish })
    expect(req).to.deep.equal(['foo', 'bar'])
  })
  it('requiredAttributes with negated parameter defined', () => {
    const barish = 'false'
    const req = requiredAttributes({ foo: true, bar: !barish })
    expect(req).to.deep.equal(['foo'])
  })
})

describe('checkAttributes tests', () => {
  var log = []
  const logContext = { logger: { warn: (msg) => log.push(msg) }, use: { useId: 0 } }

  it('checkAttributes all present', () => {
    const logged = log = []
    expect(checkAttributes(['foo', 'bar'], { foo: 'foo', bar: 'foo' }, logContext)).to.equal(true)
    expect(logged.length).to.equal(0)
  })

  it('checkAttributes one missing', () => {
    const logged = log = []
    expect(checkAttributes(['foo', 'bar'], { foo: 'foo', baz: 'foo' }, logContext)).to.equal(false)
    expect(logged.length).to.equal(1)
    expect(logged[0]).to.deep.equal({
      attributes: {
        baz: 'foo',
        foo: 'foo',
      },
      msg: "Attribute 'bar' is required",
      useId: 0,
    })
  })

  it('checkAttributes one undefined', () => {
    const logged = log = []
    expect(checkAttributes(['foo', 'bar'], { foo: 'foo', bar: undefined }, logContext)).to.equal(false)
    expect(logged.length).to.equal(1)
    expect(logged[0]).to.deep.equal({
      attributes: {
        bar: undefined,
        foo: 'foo',
      },
      msg: "Attribute 'bar' is required",
      useId: 0,
    })
  })

  it('checkAttributes one falsy', () => {
    const logged = log = []
    expect(checkAttributes(['foo', 'bar'], { foo: 'foo', bar: '' }, logContext)).to.equal(false)
    expect(logged.length).to.equal(1)
    expect(logged[0]).to.deep.equal({
      attributes: {
        bar: '',
        foo: 'foo',
      },
      msg: "Attribute 'bar' is required",
      useId: 0,
    })
  })

  it('checkAttributes two missing', () => {
    const logged = log = []
    expect(checkAttributes(['foo', 'bar'], { fooish: 'foo', barish: 'bar' }, logContext)).to.equal(false)
    expect(logged.length).to.equal(2)
    expect(logged[0]).to.deep.equal({
      attributes: {
        barish: 'bar',
        fooish: 'foo',
      },
      msg: "Attribute 'foo' is required",
      useId: 0,
    })
    expect(logged[1]).to.deep.equal({
      attributes: {
        barish: 'bar',
        fooish: 'foo',
      },
      msg: "Attribute 'bar' is required",
      useId: 0,
    })
  })
})
