/* eslint-env mocha */
'use strict'

//Opal is apt to be needed for Antora tests, perhaps not otherwise
// IMPORTANT eagerly load Opal since we'll always be in this context; change String encoding from UTF-16LE to UTF-8
const { Opal } = require('asciidoctor-opal-runtime')
if ('encoding' in String.prototype && String(String.prototype.encoding) !== 'UTF-8') {
  String.prototype.encoding = Opal.const_get_local(Opal.const_get_qualified('::', 'Encoding'), 'UTF_8') // eslint-disable-line
}
// eslint-disable-next-line no-unused-vars
const asciidoctor = require('@asciidoctor/core')()
const reports = require('./../lib/reports')
const { expect } = require('chai')
const { formatFunction } = require('../lib/reports')

function mockDoQuery (dataArray, receivedArgs) {
  return function (target, attributes, fn, contentCatalog, file, logContext, contentExtractor, transform) {
    receivedArgs.push(...arguments)
    dataArray.forEach((item) => fn(item))
  }
}

function mockBlock (blocks = []) {
  return {
    blocks,

    document: {
      hasAttribute: () => false,
      getAttribute: () => undefined,
    },
  }
}

function mockExtensions () {
  const extensions = {
    // name: undefined,

    named: (name) => {
      extensions.name = name
    },

    $option: (name, value) => {
      extensions[name] = value
    },

    onContext: (contexts) => {
      extensions.contexts = contexts
    },

    // fn: undefined,

    process: (f) => {
      extensions.fn = f
    },

    positionalAttributes: (attr) => {
      extensions.attr = attr
    },

    createList: (parent, type) => {
      const list = mockBlock([])
      list.type = type
      return list
    },

    createListItem: (list, content) => {
      const item = mockBlock([])
      item.blocks.push(content)
      return item
    },

    createInline: (parent, style, content) => {
      const item = mockBlock([])
      item.blocks.push(content)
      return item
    },

  }
  return extensions
}

describe('mock tests', () => {
  it('mockDoQuery', () => {
    const results = []
    const args = []
    const doQuery = mockDoQuery(['a', 'b'], args)
    doQuery(null, null, (item) => results.push(item), null, null, null)
    expect(results.length).to.equal(2)
    expect(results).to.deep.equal(['a', 'b'])
    expect(args.length).to.equal(6)
  })

  it('mockBlock', () => {
    const blocks = ['a', 'b']
    const b = mockBlock(blocks)
    expect(b.blocks).to.equal(blocks)
  })

  it('mockExtensions', () => {
    const extensions = mockExtensions()
    extensions.named('fooTable')
    expect(extensions.name).to.equal('fooTable')
    extensions.$option('format', 'short')
    expect(extensions.format).to.equal('short')
    extensions.positionalAttributes(['a', 'b'])
    expect(extensions.attr).to.deep.equal(['a', 'b'])
    extensions.process(() => 'foo')
    expect(extensions.fn()).to.equal('foo')
  })
})

describe('mock extension tests', () => {
  const doQuery = (args) => mockDoQuery([], args)
  const report = (args) => reports('mock', doQuery(args), {}, undefined)
  const asText = function () { return 'text' }
  reports._private.addContentExtractor({ text: asText })

  let args
  let blocks
  let parent
  let target

  beforeEach(() => {
    args = []
    blocks = []
    parent = mockBlock(blocks)
    target = 'target'
  })

  function checkArgs () {
    expect(args.length).to.equal(8)
    expect(args[2]).to.be.a('function')
    expect(args[5].use.useId).to.equal(0)
    expect(args[6]).to.equal(asText)
    expect(args[7]).to.be.a('function')
  }

  ;[true, false].forEach((targetQuery) => {
    it(`block test: targetQuery: ${targetQuery}`, () => {
      const extensions = mockExtensions()
      let reports
      extensions.mockBlockProcessor = (reports = report(args)).templateBlockProcessor('mockBlock', formatFunction, reports.processBlocks, targetQuery)
      extensions.mockBlockProcessor()
      expect(extensions.name).to.equal('mockBlock')
      expect(extensions.contexts).to.deep.equal(['listing', 'open', 'paragraph'])
      expect(extensions.attr).to.deep.equal(targetQuery ? ['target', 'query', 'formats', 'requires'] : ['formats', 'requires'])
      expect(extensions.fn).to.be.a('function')
      extensions.fn(parent, { $read_lines: () => ['foo'] }, {
        target: 'target',
        query: 'query',
        formats: 's',
        contentAs: 'text',
        transform: 'util.transform',
        requires: 'util=test/js/util.js',
      })
      checkArgs()
    })
  })

  ;[true, false].forEach((queryAttribute) => {
    [true, false].forEach((short) => {
      it(`descriptionlist test: queryAttribute: ${queryAttribute}, short: ${short}`, () => {
        const extensions = mockExtensions()
        extensions.mockList = report(args).descriptionListProcessor('mockDList', formatFunction, undefined, queryAttribute, short)
        extensions.mockList()
        expect(extensions.name).to.equal('mockDList')
        expect(extensions.format).to.equal(short ? 'short' : undefined)
        expect(extensions.attr).to.deep.equal(queryAttribute
          ? ['query', 'subjectformat', 'descriptionformat', 'requires']
          : ['subjectformat', 'descriptionformat', 'requires'])
        expect(extensions.fn).to.be.a('function')
        extensions.fn(parent, target, {
          query: 'query',
          subjectformat: 's',
          descriptionformat: 'd',
          contentAs: 'text',
          transform: 'util.transform',
          requires: 'util=test/js/util.js',
        })
        checkArgs()
      })

      it(`inline count test: query: ${queryAttribute}, short: ${short}`, () => {
        const extensions = mockExtensions()
        extensions.mockInlineCountProcessor = report(args).inlineCountProcessor('mockCount', queryAttribute, short)
        extensions.mockInlineCountProcessor()
        expect(extensions.name).to.equal('mockCount')
        expect(extensions.format).to.equal(short ? 'short' : undefined)
        expect(extensions.attr).to.deep.equal(queryAttribute ? ['query'] : undefined)
        expect(extensions.fn).to.be.a('function')
        extensions.fn(parent, target, {
          query: 'query',
          contentAs: 'text',
          transform: 'util.transform',
          requires: 'util=test/js/util.js',
        })
        checkArgs()
      })

      it(`block expression test: query: ${queryAttribute}, short: ${short}`, () => {
        const extensions = mockExtensions()
        let reports
        extensions.mockInlineExpressionProcessor = (reports = report(args)).expressionProcessor('mockExpression', formatFunction, reports.evaluateExpressionBlock, queryAttribute, short)
        extensions.mockInlineExpressionProcessor()
        expect(extensions.name).to.equal('mockExpression')
        expect(extensions.format).to.equal(short ? 'short' : undefined)
        expect(extensions.attr).to.deep.equal(queryAttribute ? ['query', 'format'] : ['format'])
        expect(extensions.fn).to.be.a('function')
        //Too hard to mock
        // extensions.fn(parent, target, {
        //   query: 'query',
        //   format: 'format',
        //   contentAs: 'text',
        //   transform: 'util.transform',
        //   requires: 'util=test/js/util.js'
        // })
        // checkArgs()
      })

      it(`inline expression test: query: ${queryAttribute}, short: ${short}`, () => {
        const extensions = mockExtensions()
        let reports
        extensions.mockInlineExpressionProcessor = (reports = report(args)).expressionProcessor('mockExpression', formatFunction, reports.evaluateExpressionInline, queryAttribute, short)
        extensions.mockInlineExpressionProcessor()
        expect(extensions.name).to.equal('mockExpression')
        expect(extensions.format).to.equal(short ? 'short' : undefined)
        expect(extensions.attr).to.deep.equal(queryAttribute ? ['query', 'format'] : ['format'])
        expect(extensions.fn).to.be.a('function')
        extensions.fn(parent, target, {
          query: 'query',
          format: 'format',
          contentAs: 'text',
          transform: 'util.transform',
          requires: 'util=test/js/util.js',
        })
        checkArgs()
      })

      it(`inline unique count test: query: ${queryAttribute}, short: ${short}`, () => {
        const extensions = mockExtensions()
        extensions.mockInlineUniqueCountProcessor = report(args).inlineUniqueCountProcessor('mockUniqueCount', formatFunction, queryAttribute, short)
        extensions.mockInlineUniqueCountProcessor()
        expect(extensions.name).to.equal('mockUniqueCount')
        expect(extensions.format).to.equal(short ? 'short' : undefined)
        expect(extensions.attr).to.deep.equal(queryAttribute ? ['query', 'format', 'requires'] : ['format', 'requires'])
        expect(extensions.fn).to.be.a('function')
        extensions.fn(parent, target, {
          query: 'query',
          format: 'format',
          contentAs: 'text',
          transform: 'util.transform',
          requires: 'util=test/js/util.js',
        })
        checkArgs()
      })

      it(`list test: queryAttribute: ${queryAttribute}, short: ${short}`, () => {
        const extensions = mockExtensions()
        extensions.mockList = report(args).listProcessor('mockList', 'ulist', '*', formatFunction, undefined, queryAttribute, short)
        extensions.mockList()
        expect(extensions.name).to.equal('mockList')
        expect(extensions.format).to.equal(short ? 'short' : undefined)
        expect(extensions.attr).to.deep.equal(queryAttribute ? ['query', 'format', 'requires'] : ['format', 'requires'])
        expect(extensions.fn).to.be.a('function')
        extensions.fn(parent, target, {
          query: 'query',
          format: 'format',
          contentAs: 'text',
          transform: 'util.transform',
          requires: 'util=test/js/util.js',
        })
        checkArgs()
      })

      it(`table test: queryAttribute: ${queryAttribute}, short: ${short}`, () => {
        const extensions = mockExtensions()
        extensions.mockTable = report(args).tableProcessor('mockTable', '|', formatFunction, queryAttribute, short)
        extensions.mockTable()
        expect(extensions.name).to.equal('mockTable')
        expect(extensions.format).to.equal(short ? 'short' : undefined)
        expect(extensions.attr).to.deep.equal(queryAttribute
          ? ['query', 'cellformats', 'requires']
          : ['cellformats', 'requires'])
        expect(extensions.fn).to.be.a('function')
        const table = mockBlock([])
        table.context = 'table'
        table.columns = ['format']
        table.rows = { body: table.blocks }
        parent.blocks.push(table)
        extensions.fn(parent, target, {
          query: 'query',
          cellformats: 'format',
          contentAs: 'text',
          transform: 'util.transform',
          requires: 'util=test/js/util.js',
        })
        checkArgs()
      })
    })
  })

  //Nothing to test here for include processors
})
