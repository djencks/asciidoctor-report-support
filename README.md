# Asciidoctor report-support Extension
:version: 0.1.3-rc.2

`@djencks/asciidoctor-report-support` provides support for adding the results of queries to an asciidoc document.

WARNING:: This is based on link:https://github.com/browserify/static-eval[static-eval].
This should not be allowed to execute arbitrary user-supplied code.
For this reason, THIS LIBRARY SHOULD ONLY BE USED ON KNOWN AND TRUSTED CONTENT!.

NOTE: for more complete, better formatted README, see https://gitlab.com/djencks/asciidoctor-jsonpath/-/blob/master/README.adoc.

## Installation

Available through npm as @djencks/asciidoctor-report-support.

The project git repository is https://gitlab.com/djencks/asciidoctor-report-support

This is a library used by several querying extensions such as @djencks/asciidoctor-jsonpath.
