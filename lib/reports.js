'use strict'

const camelCaseKeys = require('camelcase-keys')
const parse = require('esprima').parse
const picomatch = require('picomatch')
const requireFromString = require('require-from-string')
const staticEval = require('static-eval')
const userRequire = require('@antora/user-require-helper')

const { computeParent } = require('./list-utils')
const $require = Symbol('$require')

//N.B.  In ` logContext`, `use` is not a verb or command, but the use or instance of the processor in the source.

module.exports = (extensionName, doQuery, pipelineConfig, registry, config) => {
  const Opal = global.Opal
  const PreprocessorReader = Opal.module(null, 'Asciidoctor').PreprocessorReader
  const loggerManager = Opal.module(null, 'Asciidoctor').LoggerManager
  const { doTemplate, doTemplateText } = require('./block')

  const debugAttributeName = extensionName + '-debug'
  const logger = loggerManager.getLogger()

  const contentCatalog = config && config.contentCatalog
  const file = (config && config.file) || {}

  const globalRequires = pipelineConfig.globalRequires || {}

  let useCount = 0

  return {
    logger,

    attributesIncludeProcessor: function (name, formatFunction, queryAttribute = true) {
      const required = requiredAttributes({ query: queryAttribute, formats: true })
      return function () {
        const self = this
        self.handles(function (target) {
          return target.startsWith(name)
        })
        self.process(function (doc, reader, target, attributes) {
          const logContext = context(extensionName, name, doc)
          if (!checkAttributes(required, attributes, logContext)) {
            return
          }
          const { formats } = attributes
          target = target.slice(name.length)
          logContext.debug && logContext.logger.debug({ target, formats, ...logContext.use })
          const { userRequires, transform, contentExtractor } = modifiers(attributes, logContext)
          const formatSpecs = commaSplit(formats)
          const formatFns = formatSpecs.reduce((accum, formatSpec) => {
            if (formatSpec.includes('=')) {
              const { name, expr } = splitOnce(formatSpec, '=')
              const nameFn = formatFunction(name, userRequires, logContext)
              const exprFn = formatFunction(expr, userRequires, logContext)
              accum.push([(item) => nameFn(item) || name, exprFn])
            } else {
              accum.push([() => formatSpec, (item) => item[formatSpec]])
            }
            return accum
          }, [])
          doQuery(target, attributes, (item) => {
            setAttributes(formatFns, item, doc)
          }, contentCatalog, file, logContext, contentExtractor, transform)
        })

        function setAttributes (formats, item, doc) {
          const headerAttributes = doc.header_attributes
          const docAttributes = doc.attributes

          formats.forEach(([nameFn, formatFn]) => {
            const name = nameFn(item).toLowerCase()
            const value = scSubs(formatFn(item))
            headerAttributes !== Opal.nil && headerAttributes['$[]='](name, value)
            docAttributes['$[]='](name, value)
          })
        }
      }
    },

    countAttributesIncludeProcessor: function (name) {
      const required = ['queries']
      return function () {
        const self = this
        self.handles(function (target) {
          return target.startsWith(name)
        })
        self.process(function (doc, reader, target, attributes) {
          const logContext = context(extensionName, name, doc)
          if (!checkAttributes(required, attributes, logContext)) {
            return
          }
          const { queries } = attributes
          target = target.slice(name.length)
          logContext.debug && logContext.logger.debug({ target, queries, ...logContext.use })
          const { transform, contentExtractor } = modifiers(attributes, logContext)
          const querySpecs = commaSplit(queries)
          const headerAttributes = doc.header_attributes
          const docAttributes = doc.attributes
          querySpecs.forEach((querySpec) => {
            if (querySpec.includes('=')) {
              const { name, expr } = splitOnce(querySpec, '=')
              let count = 0
              doQuery(target, Object.assign({}, attributes, { query: expr }),
                (item) => count++, contentCatalog, file, logContext, contentExtractor, transform)
              logContext.debug && logContext.logger.debug(
                {
                  msg: `setting ${headerAttributes !== Opal.nil && 'header and '}doc attribute ${name} to ${count}`,
                  ...logContext.use,
                })
              headerAttributes !== Opal.nil && headerAttributes['$[]='](name, count)
              docAttributes['$[]='](name, count)
            } else {
              logContext.logger.warn({ msg: `Cannot interpret expected name=query expression: ${querySpec}`, ...logContext.use })
            }
          })
        })
      }
    },

    uniqueCountAttributesIncludeProcessor: function (name, formatFunction, queryAttribute = true) {
      const required = queryAttribute ? ['query', 'formats'] : ['formats']
      return function () {
        const self = this
        self.handles(function (target) {
          return target.startsWith(name)
        })
        self.process(function (doc, reader, target, attributes) {
          const logContext = context(extensionName, name, doc)
          if (!checkAttributes(required, attributes, logContext)) {
            return
          }
          const { query, formats } = attributes
          target = target.slice(name.length)
          logContext.debug && logContext.logger.debug({ target, query, formats, ...logContext.use })
          const headerAttributes = doc.header_attributes
          const docAttributes = doc.attributes
          const { userRequires, transform, contentExtractor } = modifiers(attributes, logContext)
          const formatSpecs = commaSplit(formats)
          const counters = formatSpecs.map((formatSpec) => {
            const { name, expr } = splitOnce(formatSpec, '=')
            return { name, formatFn: formatFunction(expr, userRequires, logContext), values: new Set() }
          })
          doQuery(target, attributes,
            (item) => {
              counters.forEach(({ formatFn, values }) => {
                values.add(formatFn(item))
              })
            }, contentCatalog, file, logContext, contentExtractor, transform)
          counters.forEach(({ name, values }) => {
            const count = values.size
            logContext.debug && logContext.logger.debug(
              {
                msg: `setting ${headerAttributes !== Opal.nil && 'header and '}doc attribute ${name} to ${count}`,
                ...logContext.use,
              })
            headerAttributes !== Opal.nil && headerAttributes['$[]='](name, count)
            docAttributes['$[]='](name, count)
          })
        })
      }
    },

    descriptionListProcessor: function (name, formatFunction,
      defaultSubjectFormat = undefined, queryAttribute = true, short = false) {
      const required =
        requiredAttributes({ query: queryAttribute, subjectformat: !defaultSubjectFormat, descriptionformat: true })
      return function () {
        const self = this
        self.named(name)
        short && self.$option('format', 'short')
        queryAttribute
          ? self.positionalAttributes(['query', 'subjectformat', 'descriptionformat', 'requires'])
          : self.positionalAttributes(['subjectformat', 'descriptionformat', 'requires'])
        self.process((parent, target, attributes) => {
          const logContext = context(extensionName, name, parent)
          if (!checkAttributes(required, attributes, logContext)) {
            return
          }
          const { subjectformat = defaultSubjectFormat, descriptionformat, level, style } = attributes
          logContext.debug && logContext.logger.debug(
            {
              target,
              subjectformat,
              descriptionformat,
              level,
              style,
              ...logContext.use,
            })
          const depth = level ? Number.parseInt(level) : undefined
          const list = computeParent(parent, depth, self, 'dlist', style)
          const { userRequires, transform, contentExtractor } = modifiers(attributes, logContext)
          const subjectFormatFn = formatFunction(subjectformat, userRequires, logContext)
          const descriptionFormatFn = formatFunction(descriptionformat, userRequires, logContext)

          doQuery(target, attributes, (item) => {
            const listTerm = self.createListItem(list, subjectFormatFn(item))
            const listItem = self.createListItem(list, descriptionFormatFn(item))
            list.blocks.push([[listTerm], listItem])
          }, contentCatalog, file, logContext, contentExtractor, transform)
        })
      }
    },

    evaluateExpressionInline: function (self, parent, target, attributes, writer, expressionFormatFn,
      logContext, contentExtractor, transform) {
      const lines = []
      doQuery(target, attributes, (item) => {
        lines.push(writer(expressionFormatFn(item)))
      }, contentCatalog, file, logContext, contentExtractor, transform)
      return self.createInline(parent, 'quoted', lines.join('\n'))
    },

    evaluateExpressionBlock: function (self, parent, target, attributes, writer, expressionFormatFn,
      logContext, contentExtractor, transform) {
      const lines = []
      doQuery(target, attributes, (item) => {
        lines.push(...writer(expressionFormatFn(item)).split('\n'))
      }, contentCatalog, file, logContext, contentExtractor, transform)
      const reader = PreprocessorReader.$new(parent.document, lines)
      self.parseContent(parent, reader, Opal.hash2(Object.keys(attributes), attributes))
    },

    expressionProcessor: function (name, formatFunction, evaluateExpression,
      queryAttribute = true, short = false, writerFunction = (_) => (text) => text) {
      const required = requiredAttributes({ query: queryAttribute, format: true })
      return function () {
        const self = this
        self.named(name)
        short && self.$option('format', 'short')
        queryAttribute ? self.positionalAttributes(['query', 'format'])
          : self.positionalAttributes(['format'])
        self.process(function (parent, target, attributes) {
          const logContext = context(extensionName, name, parent)
          if (!checkAttributes(required, attributes, logContext)) {
            return
          }
          const { format, outputFormat = 'text' } = attributes
          logContext.debug && logContext.logger.debug({ target, format, outputFormat, ...logContext.use })
          // const writer = writers[outputFormat]
          const writer = writerFunction(outputFormat, logContext)
          if (!writer) {
            return
          }
          const { userRequires, transform, contentExtractor } = modifiers(attributes, logContext)
          const expressionFormatFn = formatFunction(format, userRequires, logContext)
          return evaluateExpression(self, parent, target, attributes, writer, expressionFormatFn,
            logContext, contentExtractor, transform)
        })
      }
    },

    inlineCountProcessor: function (name, queryAttribute = true, short = false) {
      const required = requiredAttributes({ query: queryAttribute })
      return function () {
        const self = this
        self.named(name)
        short && self.$option('format', 'short')
        queryAttribute && self.positionalAttributes(['query'])
        self.process(function (parent, target, attributes) {
          const logContext = context(extensionName, name, parent)
          if (!checkAttributes(required, attributes, logContext)) {
            return
          }
          logContext.debug && logContext.logger.debug({ target, ...logContext.use })
          const { transform, contentExtractor } = modifiers(attributes, logContext)
          let count = 0
          doQuery(target, attributes, (item) => count++, contentCatalog, file, logContext, contentExtractor, transform)
          return self.createInline(parent, 'quoted', count)
        })
      }
    },

    inlineUniqueCountProcessor: function (name, formatFunction, queryAttribute = true, short = false) {
      const required = requiredAttributes({ query: queryAttribute, format: true })
      return function () {
        const self = this
        self.named(name)
        short && self.$option('format', 'short')
        queryAttribute
          ? self.positionalAttributes(['query', 'format', 'requires'])
          : self.positionalAttributes(['format', 'requires'])
        self.process(function (parent, target, attributes) {
          const logContext = context(extensionName, name, parent)
          if (!checkAttributes(required, attributes, logContext)) {
            return
          }
          const { format } = attributes
          logContext.debug && logContext.logger.debug({ target, format, ...logContext.use })
          const { userRequires, transform, contentExtractor } = modifiers(attributes, logContext)
          const formatFn = formatFunction(format, userRequires, logContext)
          var values = new Set()
          doQuery(target, attributes,
            (item) => values.add(formatFn(item)), contentCatalog, file, logContext, contentExtractor, transform)
          return self.createInline(parent, 'quoted', values.size)
        })
      }
    },

    listProcessor: function (name, type, markerChar, formatFunction,
      defaultFormat = undefined, queryAttribute = true, short = false) {
      const required = requiredAttributes({ query: queryAttribute, format: !defaultFormat })
      return function () {
        const self = this
        self.named(name)
        short && self.$option('format', 'short')
        queryAttribute
          ? self.positionalAttributes(['query', 'format', 'requires'])
          : self.positionalAttributes(['format', 'requires'])
        self.process((parent, target, attributes) => {
          const logContext = context(extensionName, name, parent)
          if (!checkAttributes(required, attributes, logContext)) {
            return
          }
          const { format = defaultFormat, level, style } = attributes
          logContext.debug && logContext.logger.debug({ target, format, level, style, ...logContext.use })
          const depth = level ? Number.parseInt(level) : undefined
          const list = computeParent(parent, depth, self, type, style)
          const marker = markerChar.repeat(depth || 1)

          const { userRequires, transform, contentExtractor } = modifiers(attributes, logContext)
          const formatFn = formatFunction(format, userRequires, logContext)

          doQuery(target, attributes, (item) => {
            const listItem = self.createListItem(list, formatFn(item))
            listItem.marker = marker
            list.blocks.push(listItem)
          }, contentCatalog, file, logContext, contentExtractor, transform)
        })
      }
    },

    tableProcessor: function (name, delimiter, formatFunction, queryAttribute = true, short = false) {
      const required = requiredAttributes({ query: queryAttribute, cellformats: true })
      return function () {
        const self = this
        self.named(name)
        short && self.$option('format', 'short')
        queryAttribute ? self.positionalAttributes(['query', 'cellformats', 'requires'])
          : self.positionalAttributes(['cellformats', 'requires'])
        self.process(function (parent, target, attributes) {
          const logContext = context(extensionName, name, parent)
          let valid = checkAttributes(required, attributes, logContext)
          const table = parent.blocks[parent.blocks.length - 1]
          if (table.context !== 'table') {
            logContext.logger.warn({ msg: `jsonpathTable block macro must follow a table, not a ${table.context}`, ...logContext.use })
            valid = false
          }
          if (!valid) return
          const { cellformats } = attributes
          logContext.debug && logContext.logger.debug({ target, cellformats, ...logContext.use })
          const columns = table.columns
          const rows = table.rows.body
          const { userRequires, transform, contentExtractor } = modifiers(attributes, logContext)
          let append = false
          const cellAttrs = cellformats.split(delimiter).reduce((accum, attr) => {
            accum.push(append ? `${accum.pop().slice(0, -1)}${delimiter}${attr}` : attr)
            append = attr.endsWith('\\')
            return accum
          }, [])
          if (columns.length !== cellAttrs.length) {
            logContext.logger.warn({
              msg: `column count ${columns.length} differs from 'cellformats' attribute count ${cellAttrs.length}: cell formats:`,
              cellFormats_split: cellAttrs,
              ...logContext.use,
            })
            return
          }
          const cellFill = cellAttrs.map((cellAttr) => formatFunction(cellAttr, userRequires, logContext))

          doQuery(target, attributes, (item) => {
            //Push the row before filling it so Asciidoctor knows we are not in the header and applies column styles.
            const row = []
            rows.push(row)
            columns.forEach((col, i) => row.push(Opal.Asciidoctor.Table.Cell.$new(col, cellFill[i](item) || '')))
          }, contentCatalog, file, logContext, contentExtractor, transform)
        })
      }
    },

    processBlocks: function (target, attributes, template, parent, formatFns, contentExtractor, transform, logContext) {
      doQuery(target, attributes, (item) => {
        const innerDoc = doTemplate(registry, template, parent, attributes, formatFns, item)
        parent.blocks.push(innerDoc)
      }, contentCatalog, file, logContext, contentExtractor, transform)
    },

    processTemplates: function (target, attributes, template, parent, formatFns,
      contentExtractor, transform, logContext, processor) {
      const lines = []
      doQuery(target, attributes, (item) => {
        lines.push(...doTemplateText(registry, template, parent, attributes, formatFns, item))
      }, contentCatalog, file, logContext, contentExtractor, transform)
      const reader = PreprocessorReader.$new(parent.document, lines)
      processor.parseContent(parent, reader, Opal.hash2(Object.keys(attributes), attributes))
    },

    templateBlockProcessor: function (name, formatFunction, processFunction, targetQuery = true) {
      const required = requiredAttributes({ target: targetQuery, query: targetQuery, formats: true })
      return function () {
        const self = this
        self.named(name)
        self.onContext(['listing', 'open', 'paragraph'])
        targetQuery
          ? self.positionalAttributes(['target', 'query', 'formats', 'requires'])
          : self.positionalAttributes(['formats', 'requires'])
        self.process(function (parent, reader, attributes) {
          const logContext = context(extensionName, name, parent)
          if (!checkAttributes(required, attributes, logContext)) {
            return
          }
          const { target, formats } = attributes
          logContext.debug && logContext.logger.debug({ target, formats, ...logContext.use })
          //unescape ifeval, endif, include, ifdef
          const template = reader.$read_lines().map((line) => line.replace(/^%(%*ifeval::|%*endif::|%*include::|%*ifdef::)/g, '$1')).join('\n')

          logContext.debug && logContext.logger.debug({ template, ...logContext.use })
          const { userRequires, transform, contentExtractor } = modifiers(attributes, logContext)
          const formatSpecs = commaSplit(formats)
          const formatFns = formatSpecs.reduce((accum, formatSpec) => {
            if (formatSpec.includes('=')) {
              const { name, expr } = splitOnce(formatSpec)
              accum[name.toLowerCase()] = formatFunction(expr, userRequires, logContext)
            } else {
              accum[formatSpec] = formatFunction(formatSpec, userRequires, logContext)
            }
            return accum
          }, {})
          processFunction(target, attributes, template, parent, formatFns,
            contentExtractor, transform, logContext, self)
        })
      }
    },
  }

  function context (extensionName, name, parent) {
    const use = { useId: useCount++ }
    use[extensionName] = name
    return { logger, debug: getDebug(parent), use }
  }

  function getDebug (parent) {
    const debugSet = parent.document.hasAttribute(debugAttributeName)
    return debugSet ? parent.document.getAttribute(debugAttributeName) : pipelineConfig.debug
  }

  function modifiers (attributes, logContext) {
    const { requires, contentAs, transform: transformSpec } = attributes
    const userRequires = createUserRequires(requires, pipelineConfig.playbookDir, globalRequires,
      file.src, contentCatalog, logContext)
    const transform = getTransform(userRequires, transformSpec)
    const contentExtractor = getContentExtractor(contentAs, logContext)
    return { userRequires, transform, contentExtractor }
  }
}

function createUserRequires (requires, playbookDir, parentRequires, context, contentCatalog, logContext) {
  if (requires) {
    const requiresList = requiresToList(requires)
    const userRequireContext = playbookDir ? { dot: playbookDir, paths: [playbookDir || '', __dirname] } : {}
    return requiresList.reduce((accum, { name, expr }) => {
      try {
        if (expr.startsWith('xref:')) {
          const spec = expr.slice('xref:'.length)
          const file = contentCatalog.resolveResource(spec, context, 'example')
          if (file) {
            let required = file[$require]
            if (!required) {
              required = requireFromString(file.contents.toString(), file.path)
              file[$require] = required
            }
            accum[name] = required
          } else {
            logContext.logger.warn({ msg: `Could not locate '${expr}' in contentCatalog`, context, ...logContext.use })
          }
        } else {
          accum[name] = userRequire(expr, userRequireContext)
        }
      } catch (e) {
        logContext.logger.warn({ msg: `Could not require '${expr}', error: ${e}`, ...logContext.use })
      }
      return accum
    }, Object.assign({}, parentRequires))
  }
  return parentRequires || {}
}

function getTransform (requires, transformSpec) {
  if (requires && transformSpec) {
    return transformSpec.split('.').reduce((accum, name) => accum && accum[name], requires)
  }
}

function requiresToList (requires) {
  return requires ? requires.split(',').map((term) => splitOnce(term)) : requires
}

function commaSplit (expr) {
  return expr
    ? expr.split(/(?<!\\),/).map((term) => term.trim().split('\\,').join(','))
    : []
}

function splitOnce (querySpec, token = '=') {
  const index = querySpec.indexOf(token)
  const name = querySpec.substring(0, index).trim()
  const expr = querySpec.substring(index + 1).trim()
  return { name, expr }
}

function formatFunction (text, userRequires, logContext, enhanceItem = (item) => item) {
  try {
    const expr = parse(text).body[0].expression
    return (item) => {
      try {
        return staticEval(expr,
          Object.assign({ Object: Object },
            userRequires, enhanceItem(item)), { allowAccessToMethodsOnFunctions: true })
      } catch (e) {
        logContext.logger.warn({ msg: `Could not evaluate '${text}'`, item, e, ...logContext.use })
        return `could not evaluate '${text}'\nError: ${e}\n${JSON.stringify(e)}`
      }
    }
  } catch (e) {
    logContext.logger.warn({ msg: `Could not parse '${text}', error: ${e.message}`, ...logContext.use })
    return (item) => e.message
  }
}

function requiredAttributes (info) {
  return Object.entries(info).filter(([name, value]) => value).map(([name, value]) => name)
}

function checkAttributes (required, attributes, logContext) {
  return required.reduce((valid, name) => {
    if (!attributes[name]) {
      logContext.logger.warn({ msg: `Attribute '${name}' is required`, attributes, ...logContext.use })
      return false
    }
    return valid
  }, true)
}

const TRUE = () => true

//this needs to be global since e.g. indexer can't know about json or xml extractors.
const contentExtractors = {}

function addContentExtractor (contentAs) {
  contentAs && Object.assign(contentExtractors, contentAs)
}

function pipelineConfigure (registry, config, register, doQuery, name,
  enhanceTargetItem = undefined, contentAs = undefined) {
  if (registry.on && config.playbook) {
    //registering Asciidoctor extensions from the Antora extension
    registry.on('contentClassified', ({ siteAsciiDocConfig }) => {
      siteAsciiDocConfig.extensions ? siteAsciiDocConfig.extensions.push(register)
        : siteAsciiDocConfig.extensions = [register]
    })

    const logger = registry.getLogger(name)
    const indexerConfig = config.config || {}

    indexerConfig.logLevel && (logger.level = indexerConfig.logLevel)
    const debug = logger.isLevelEnabled('debug')
    const trace = logger.isLevelEnabled('trace')
    let useCount = 0
    const logContext = { logger, debug, trace, use: { useId: useCount++, indexer: 'indexPage' } }

    let indexPages = indexerConfig.indexPages &&
      indexerConfig.indexPages.map((indexPage) => Object.assign({}, indexPage))
    indexPages && logContext.debug && logContext.logger.debug({ indexPages, ...logContext.use })

    addContentExtractor(contentAs)
    logContext.logger.info({ contentExtractors: Object.keys(contentExtractors), ...logContext.use })

    const globalRequires = createUserRequires(indexerConfig.requires, config.playbook.dir, {},
      {}, undefined, logContext)

    if (indexPages) {
      enhanceTargetItem || (enhanceTargetItem = () => (item) => {
        return { item, path: '' }
      })

      registry.on('contentAggregated', ({ contentAggregate }) => {
        indexPages = indexPages.reduce((accum, indexPage) => {
          if (indexPage.local) {
            const local = indexPage.local
            logContext.logger.debug({ msg: 'local indexPage specification', local, ...logContext.use })
            const contentExtractor = contentExtractors[local.contentAs || 'json']
            if (!contentExtractor) {
              logContext.logger.info(`no content extractor for local index page configuration in format ${local.contentAs || 'json'}`, logContext.use)
              return accum
            }
            const nameMatcher = local.component ? picomatch(local.component) : TRUE
            const versionMatcher = local.version ? picomatch(local.version) : TRUE
            contentAggregate.forEach((componentVersionData) => {
              const { name, version } = componentVersionData
              if (nameMatcher(name) && versionMatcher(version)) {
                logContext.logger.debug({ msg: 'matched local', componentName: name, version, ...logContext.use })
                for (const file of componentVersionData.files) {
                  if (local.path === file.path) {
                    const localPages = camelCaseKeys(contentExtractor(file, logContext), { deep: true })
                    ;(Array.isArray(localPages) ? localPages : localPages.indexPages).forEach((localPage) => {
                      accum.push(Object.assign(localPage,
                        { query: Object.assign(localPage.query, { component: name, version }) }))
                    })
                    break
                  }
                }
              }
            })
          } else {
            accum.push(indexPage)
          }
          return accum
        }, [])
        logContext.logger.debug({ indexPages, ...logContext.use })
      })

      registry.on('contentClassified', ({ contentCatalog }) => {
        indexPages.forEach((indexPage) => {
          logContext.use.useId = useCount++
          logContext.logger.debug({ indexPage, ...logContext.use })
          //TODO check for query, target, template_id
          const attributes = Object.assign({}, indexPage.query)
          const base = baseFilter({}, attributes)

          const contentExtractor = getContentExtractor(indexPage.contentAs, logContext)
          const templateId = shortenSrc(indexPage.templateId)
          const targetQuery = Object.assign({}, base, templateId)
          const templateFiles = contentCatalog.findBy(targetQuery)
          if (!templateFiles.length) {
            logContext.logger.warn({ msg: 'template file not found in content catalog', targetQuery, ...logContext.use })
            return
          }
          templateFiles.forEach((templateFile) => {
            logContext.use.useId = useCount++
            logContext.debug && logContext.logger.debug({
              msg: 'template file located',
              templateSrc: shortenSrc(templateFile.src),
              ...logContext.use,
            })
            const target = Object.assign({}, shortenSrc(templateFile.src), { family: 'page' }, indexPage.target)
            const file = { src: target }
            const userRequires = createUserRequires(indexPage.requires, config.playbook.dir, globalRequires,
              target, contentCatalog, logContext)
            const transform = getTransform(userRequires, indexPage.transform)
            const targetMapper = mapItemTarget(indexPage.target, userRequires, logContext, enhanceTargetItem)
            const template = '`' + templateFile.contents.toString() + '`'
            const format = formatFunction(template, userRequires, logContext)
            const extract = indexPage.extract
            const extractFn = extract ? extractor(extract) : (item) => item
            let itemCount = 0
            doQuery(indexPage.query.target, attributes, (item) => {
              item = extractFn(item)
              const pageText = format(item)
              const { relative, path } = targetMapper(item)
              const targetFile = {
                contents: Buffer.from(pageText),
                src: Object.assign({ module: 'ROOT', family: 'page' }, target, { relative, path }),
                path,
              }
              const added = contentCatalog.addFile(targetFile)
              itemCount++
              trace && logger.trace({
                msg: 'Added  page',
                target: {
                  src: shortenSrc(added.src),
                  outPath: added.out.path,
                  pubUrl: added.pub.url,
                },
                ...logContext.use,
              })
            }, contentCatalog, file, logContext, contentExtractor, transform)
            logContext.debug && logger.debug({ msg: `added ${itemCount} pages`, ...logContext.use })
          })
        })
        // doQuery isn't used again and may have a large cache
        doQuery = undefined
      })
    }

    return { playbookDir: config.playbook.dir, globalRequires, debug, trace }
  }
}

function mapItemTarget ({ format, match }, userRequires, logContext, enhanceItemBuilder) {
  if (format) {
    const enhanceItem = enhanceItemBuilder(match)
    const formatter = formatFunction(format, userRequires, logContext)
    return (item0) => {
      const { item, path } = enhanceItem(item0)
      return { relative: formatter(item), path }
    }
  }
  logContext.logger.warn({ msg: 'no format supplied in indexPage', ...logContext.use })
  return (item) => {
    return { relative: 'unspecified.adoc', path: '' }
  }
}

function shortenSrc (src) {
  return ['component', 'version', 'module', 'family', 'relative'].filter((key) => key in src)
    .reduce((accum, key) => {
      accum[key] = src[key]
      return accum
    }, {})
}

function baseFilter (src, attrs) {
  const { component, version, module } = src
  const filter = {}
  attrs.component === '*' || (attrs.component && (filter.component = attrs.component)) ||
  (component && (filter.component = component))

  attrs.version === '*' || (attrs.version === '~' && ((filter.version = '') || true)) ||
  (attrs.version && (filter.version = attrs.version)) ||
  (version && (filter.version = version))

  attrs.module === '*' || (attrs.module && (filter.module = attrs.module)) ||
  (module && (filter.module = module))

  attrs.family && (filter.family = attrs.family)

  return filter
}

function extractor (extract) {
  const matchers = extract.map(({ path, match }) => {
    return {
      matcher: picomatch(match),
      path: path.split('.'),
    }
  })
  return (item) => {
    item = Object.assign({}, item)
    matchers.forEach(({ path, matcher }) => {
      const value = path.reduce((accum, segment) => {
        return accum[segment]
      }, item)
      const match = matcher(value, true)
      if (match.isMatch) {
        Object.assign(item, match.match.groups)
      }
    })
    return item
  }
}

function getContentExtractor (contentAs, logContext) {
  const contentExtractor = contentExtractors[contentAs]
  if (contentAs && !contentExtractor) { logContext.logger.warn({ msg: `contentAs processor ${contentAs} not found`, ...logContext.use }) }
  return contentExtractor
}

const SPECIAL_CHARS = /[<>&]/g

const REPLACEMENTS = {
  '<': '&lt;',
  '>': '&gt;',
  '&': '&amp;',
}

function scSubs (string) {
  return string ? string.replace(SPECIAL_CHARS, (m) => REPLACEMENTS[m]) : string
}

module.exports.baseFilter = baseFilter
module.exports.formatFunction = formatFunction
module.exports.pipelineConfigure = pipelineConfigure
module.exports.splitOnce = splitOnce
module.exports._private = { requiredAttributes, checkAttributes, extractor, scSubs, addContentExtractor }
