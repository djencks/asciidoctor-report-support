'use strict'

const Opal = global.Opal
const Document = Opal.module(null, 'Asciidoctor').Document
const attributeEntry = Document.AttributeEntry
const PreprocessorReader = Opal.module(null, 'Asciidoctor').PreprocessorReader
const Cursor = Opal.module(null, 'Asciidoctor').Reader.Cursor
var nil = Opal.nil
var $truthy = Opal.truthy
var $hash2 = Opal.hash2

const EMPTY = $hash2([], {})

const $precedingAttributes = Symbol('precedingAttributes')

const innerDocument = (() => {
  const scope = Opal.klass(
    Opal.module(null, 'reportSupport', function () {
    }),
    Document,
    'innerDocument',
    function () {
    }
  )
  Opal.defn(scope, '$initialize', function initialize (text, opts, precedingAttributes) {
    const headerAttributes = opts['$[]']('parent').attributes
    Opal.send(this, Opal.find_super_dispatcher(this, 'initialize', initialize), ['', opts])
    //TODO why is this needed? I thought the subdocument attributes would be the parent attributes as of construction.
    this.attributes = this.attributes.$merge(headerAttributes)
    this[$precedingAttributes] = precedingAttributes
    const self = this
    self.parsed = Opal.nil
    self.reader = PreprocessorReader.$new(self, text, Cursor.$new(self.attributes['$[]']('docfile'), self.base_dir), $hash2(['normalize'], { normalize: true }))
    if ($truthy(self.sourcemap)) {
      return (self.source_location = self.reader.$cursor())
    } else {
      return nil
    }
  })
  Opal.defn(scope, '$convert', function $$convert (opts) {
    var self = this
    self.parent_document.$playback_attributes(self[$precedingAttributes])
    return Opal.send(this, Opal.find_super_dispatcher(this, 'convert', $$convert), [opts])
  })
  //expect '|' table separator
  Opal.def(scope, '$nested?', function () {
    return false
  })
  return scope
})()

function prepareAttributes (attributes, formats, item, parent) {
  //precedingAttributeEntries are the attributes defined between the previous block and this block
  const precedingAttributeEntries = attributes.attribute_entries
  delete attributes.attribute_entries
  const precedingAttributes = Opal.hash2(['attribute_entries'], { attribute_entries: precedingAttributeEntries })
  //Turn attributes into a hash suitable for playbackAttributes
  const attrHash = Opal.hash2(['attribute_entries'],
    {
      attribute_entries: Object.entries(formats).map(([name, formatFn]) => {
        const value = formatFn(item)
        return attributeEntry.$new(name, value, false)
      }).concat(attributeEntry.$new('fragment', '', false)),
    })

  parent.document.$playback_attributes(precedingAttributes)
  //Save attributes up to here, including preceding attributes
  const originalAttributes = parent.document.$attributes().$merge(EMPTY)
  //apply attributes for this template
  parent.document.$playback_attributes(attrHash)
  return { precedingAttributes, originalAttributes }
}

function makeInnerDoc (registry, parent, template, precedingAttributes, originalAttributes) {
  const options = Opal.hash2(['standalone', 'extension_registry', 'parent'],
    {
      standalone: false,
      extension_registry: registry,
      parent: parent.document,
    })
  const innerDoc = innerDocument.$new(template, options, precedingAttributes).parse()
  parent.document.attributes = originalAttributes
  return innerDoc
}

module.exports.doTemplate = (registry, template, parent, attributes, formats, item) => {
  const { precedingAttributes, originalAttributes } = prepareAttributes(attributes, formats, item, parent)
  return makeInnerDoc(registry, parent, template, precedingAttributes, originalAttributes)
}

function applySubs (parent, attributes, template, type, name) {
  const subs = attributes.subs || 'attributes'
  template = parent.applySubstitutions(template, parent.$resolve_subs(subs, type, ['attributes'], name))
  return template
}

module.exports.doTemplateText = (registry, template, parent, attributes, formats, item) => {
  const { originalAttributes } = prepareAttributes(attributes, formats, item, parent)
  const lines = applySubs(parent, attributes, template, 'block', 'indexBlock').split('\n')
  parent.document.attributes = originalAttributes
  return lines
}
