'use strict'

function isList (parent) {
  return ['ulist', 'olist', 'dlist'].includes(parent.context)
}

//computeParent locates or creates the list to add list items to.
function computeParent (parent, depth, self, type, style, logger, useLogContext, trace) {
  if (depth && parent.blocks.length) {
    const list = parent.blocks[parent.blocks.length - 1]
    if (depth === 1 && list.context === type) {
      return list
    }
    if (depth > 1 && isList(list)) {
      return locateList(list, depth)
    }
  }
  return newList(parent)

  function newList (parent) {
    trace && logger.debug({ msg: `new ${type} in context ${parent.context}`, ...useLogContext })
    const list = self.createList(parent, type)
    style && (list.style = style)
    parent.blocks.push(list)
    return list
  }

  function locateList (parent, depth) {
    if (!isList(parent)) {
      throw new Error(`locateList must be called with a list, not ${parent.context}`)
    }

    var last = parent.blocks[parent.blocks.length - 1]
    //dlist items are array [[term1,...],item].
    //A sublist is a block of item.
    if (Array.isArray(last)) {
      if (last.length !== 2) {
        logger.warn({ msg: `unexpected array ${last} in ${parent}`, ...useLogContext })
        return
      }
      last = last[1]
    }
    //last is a list_item. Does it already contain an appropriate list as the last block?
    if (last.blocks && last.blocks.length) {
      const lastBlock = last.blocks[last.blocks.length - 1]
      if (isList(lastBlock)) {
        if (depth === 2) {
          //We have reached the requested depth.
          if (lastBlock.context === type) {
            //There is already a list of the correct type at the requested depth.
            return lastBlock
          }
          //create a new list of the correct type at the requested depth.
          return newList(last)
        }
        //Search deeper.
        return locateList(lastBlock, depth - 1)
      }
    }
    //There are no more sublists, but we haven't reached the requested depth.
    //Create a sublist at the deepest existing depth.
    return newList(last)
  }
}

function print (block, level = 0) {
  if (block.context) {
    console.log(`level: ${level}, context: ${block.context}, id: ${block.$$id}, block count: ${block.blocks.length}, style: ${block.style}`)
    level++
    block.blocks.forEach((sub) => print(sub, level))
  } else if (Array.isArray(block)) {
    console.log(`level: ${level}, encountered array of size ${block.length}`)
    block.forEach((sub) => print(sub, level))
    console.log('exiting array')
  } else {
    console.log(`unknown object at level ${level}`, block)
  }
}

module.exports.computeParent = computeParent
module.exports.print = print
